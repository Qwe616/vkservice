﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Safari;

namespace VkService.App.Base
{

    public class WebDriverFactory
    {
        private static int _implicitWaitTimeout;
        private static string _browser;
        private static string _directory;


        public static void Register(string browser,int timewait)
        {
            _implicitWaitTimeout = timewait;
            _browser = browser;
        }
        private WebDriverFactory()
        {
        }

        private IList<Cookie> _cookies;

        public static IWebDriver GetDriver()
        {
            IWebDriver driver;
            _directory = Path.GetDirectoryName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory))+@"\packages\";

            switch (_browser.ToUpper())
            {
                case "FIREFOX":
                    var path =
                        FirefoxDriverService.CreateDefaultService(
                            _directory + @"Selenium.Mozilla.Firefox.Webdriver.0.6.0.1\Driver");
                    driver = new FirefoxDriver(path);
                    break;
                case "CHROME":
                    driver = new ChromeDriver(_directory + @"Selenium.WebDriver.ChromeDriver.2.27.0\driver");
                    break;
                case "IE10":
                    var options = new InternetExplorerOptions
                    {
                        IgnoreZoomLevel = true
                    };
                    var a = _directory + @"Selenium.WebDriver.IEDriver.3.0.0.1\driver";
                    driver = new InternetExplorerDriver(_directory + @"Selenium.WebDriver.IEDriver.3.0.0.1\driver", options);
                    break;
                case "SAFARI":
                    driver = new SafariDriver();
                    break;
                default:
                    throw new InvalidElementStateException("Unsupported browser type");
            }

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(_implicitWaitTimeout));
            return driver;
        }

        public static void FinishBrowser(IWebDriver driver)
        {
            if (driver == null) return;
            driver.Quit();
            driver = null;
        }

        public void RegisterCookie(IWebDriver driver)
        {
            _cookies = driver.Manage().Cookies.AllCookies;
        }

        public void AddCookiesInDriver(IWebDriver driver)
        {
            if (_cookies == null) return;
            foreach (var c in _cookies)
            {
                driver.Manage().Cookies.AddCookie(c);
            }
        }
    }
}