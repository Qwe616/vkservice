﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace VkService.App.Pages
{
    public class LoginPage
    {
        private const string PageUrl = "https://vk.com";

        [FindsBy(How = How.Id, Using = UserNameTextField)]
        private IWebElement _inputLogin;

        [FindsBy(How = How.Id, Using = PasswordTextField)]
        private IWebElement _inputPassword;

        [FindsBy(How = How.Id, Using = LoginButton)]
        private IWebElement _buttonLoggin;

        private readonly IWebDriver _driver;

        public LoginPage(IWebDriver driver)
        {
            _driver = driver;
            OpenPage();
            PageFactory.InitElements(_driver, this);
        }

        protected void OpenPage()
        {
            _driver.Navigate().GoToUrl(PageUrl);
        }

        public bool IsPageOpened()
        {
            return _inputLogin.Displayed && _inputPassword.Displayed && _buttonLoggin.Displayed;
        }

        public LoginPage TypeUserName(string userName)
        {
            _inputLogin.SendKeys(userName);
            return this;
        }

        public LoginPage TypePassword(string password)
        {
            _inputPassword.SendKeys(password);
            return this;
        }

        public MainPage ClickLoginButton()
        {
            _buttonLoggin.Click();
            return new MainPage(_driver);
        }

        public const string UserNameTextField = "index_email";
        public const string PasswordTextField = "index_pass";
        public const string LoginButton = "index_login_button";
    }
}