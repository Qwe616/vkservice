﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace VkService.App.Pages
{
    public class FriendsListPage
    {
        [FindsBy(How = How.XPath, Using = FriendsLinkLocator)]
        public IList<IWebElement> FriendsLink { get; set; }

        [FindsBy(How = How.Id, Using = ShowMoreLocator)]
        private IWebElement _showMore;

        public readonly IWebDriver Driver;

        public FriendsListPage(IWebDriver driver)
        {
            Driver = driver;
            PageFactory.InitElements(Driver, this);
        }

        public FriendsListPage GetMoreFriend()
        {
            if (!_showMore.Displayed) return null;
            _showMore.Click();
            return this;
        }

        public const string FriendsLinkLocator = "//img[@class='friends_photo_img'][@src!='/images/deactivated_100.png']/..";
        public const string ShowMoreLocator    = "show_more";
    }
}