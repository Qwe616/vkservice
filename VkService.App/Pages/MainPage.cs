﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace VkService.App.Pages
{
    public class MainPage
    {
        protected string PageUrl = "https://vk.com";

        [FindsBy(How = How.ClassName, Using = UserNameAndSurnameLocator)]
        public IWebElement UserNameAndSurname;

        [FindsBy(How = How.Id, Using = UserStatusLocator)]
        public IWebElement UserStatus;

        [FindsBy(How = How.XPath, Using = UserFriendsListLocator)]
        private IWebElement _userFriendsButton;

        [FindsBy(How = How.Id, Using = PageButtonLocator)]
        private IWebElement _linkMyPageMain;

        [FindsBy(How = How.XPath, Using = MatualFriendsLocator)]
        public IWebElement _MatuaFriends;

        [FindsBy(How = How.XPath, Using = WorkInfoLocator)]
        public IWebElement _WorkInfo;

        private FriendsListPage Friends { get; set; }

        public IWebDriver Driver { get; set; }

        public MainPage(IWebDriver driver,string url = null)
        {
            Driver = driver;
            PageUrl = url ?? PageUrl;
            PageFactory.InitElements(Driver,this);
            if (Driver.Url.Equals("https://vk.com/feed"))
            {
                OpenMainPage();
            }
        }

        public MainPage OpenMainPage()
        {
            _linkMyPageMain.Click();
            return new MainPage(Driver);
        }

        public MainPage OpenPage()
        {
            Driver.Navigate().GoToUrl(PageUrl);
            return this;
        }

        public FriendsListPage GoToFriendsPage()
        {
            if (!_userFriendsButton.Displayed) return null;
            _userFriendsButton.Click();
            Friends = new FriendsListPage(Driver);
            return Friends;
        }

        public const string PageButtonLocator         = "l_pr";
        public const string UserNameAndSurnameLocator = "page_name";
        public const string UserStatusLocator         = "page_current_info";
        public const string UserFriendsListLocator    = "//div[@id='profile_friends']/a[@class='module_header']";

        public const string MatualFriendsLocator      = "//h3[span[text()='Общие друзья']]/span[@class='header_count fl_l']";
        public const string WorkInfoLocator           = "//div[div[text()='Место работы:']]/div[@class='labeled']/a";
    }
}