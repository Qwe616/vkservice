﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CostEffectiveCode.Ddd;
using VkService.App.Base;
using VkService.App.Manager;
using VkService.DAL;
using VkService.DAL.Entities;

namespace VkService.App.Service
{
    public interface ILoaderFriends
    {
        void Start(string browser, int timewait, string login, string password, int iteration);
    }
    public class LoaderFriends : ILoaderFriends
    {
        private readonly IPageManager _pageManager;
        private readonly IUnitOfWork _unitOfWork;
        public static bool FlagRun;

        public LoaderFriends(IPageManager pageManager,IUnitOfWork unitOfWork )
        {
            _pageManager = pageManager;
            _unitOfWork  = unitOfWork;
        }

        public void Start(string browser,int timewait,string login,string password,int iteration)
        {
            FlagRun = true;
            WebDriverFactory.Register(browser,timewait);
            try
            {
                var users = _pageManager.FindFriends(login, password, iteration);
                if (users.Any()) LoadInDb(users);
            }
            catch (Exception e)
            {
                // ignored
            }

            FlagRun = false;
        }

        private void LoadInDb(IEnumerable<User> users)
        {
            var dbContext = _unitOfWork as VkServiceDbContext;
            if (dbContext != null)
            {
                dbContext.AddRange(users);
            }
            else
            {
                foreach (var user in users)
                {
                    _unitOfWork.Add(user);
                }

            }
            _unitOfWork.Commit();
        }
    }
}