﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using VkService.App.Base;
using VkService.App.Pages;
using VkService.DAL.Entities;

namespace VkService.App.Manager
{
    public interface IPageManager
    {
        User[] FindFriends(string name, string password, int iteration);
    }

    public class PageManager : IPageManager
    {
        private IWebDriver _driver;
        private readonly List<User> _users;

        public PageManager()
        {
            _users = new List<User>();
        }

        public User[] FindFriends(string name, string password, int iteration)
        {
            _driver = WebDriverFactory.GetDriver();
            var mainUser = Login(name, password);
            _users.Add(mainUser);
            Bfs(mainUser, iteration);
            WebDriverFactory.FinishBrowser(_driver);
            return _users.ToArray();
        }

        private void Bfs(User user, int iteration)
        {
            var queue = new Queue<User>();
            queue.Enqueue(user);
            while (queue.Count != 0)
            {
                var checkUser = queue.Dequeue();
                if (iteration == 0) break;
                var friends = GetUserFriends(checkUser);
                foreach (var friend in friends)
                {
                    queue.Enqueue(friend);
                    _users.Add(friend);
                }
                iteration--;
            }
        }

        private IEnumerable<User> GetUserFriends(User user)
        {
            return GetFriendsUrls(user.Url).ToArray().Select(url => CreateUser(url, user)).ToList();
        }

        private User Login(string name, string password)
        {
            new LoginPage(_driver)
                .TypeUserName(name)
                .TypePassword(password)
                .ClickLoginButton()
                .OpenMainPage();
            var user = CreateUser(_driver.Url);
            return user;
        }

        private User CreateUser(string url, User parent = null)
        {
            var page = NewPage(url);
            var text = page.UserNameAndSurname.Text;
            var user = new User
            {
                Name = text.Substring(0, text.IndexOf(" ", StringComparison.Ordinal)).Trim(),
                Surname = text.Substring(text.LastIndexOf(" ", StringComparison.Ordinal)).Trim(),
                Url = page.Driver.Url.Trim(),
                Parent = parent,
                Login = new Regex("^https://vk.com/(id)?(?<login>.*)")
                    .Match(page.Driver.Url)
                    .Groups["login"]
                    .Value.Trim()
            };
            if (page.Driver.FindElements(By.Id(MainPage.UserStatusLocator)).Count != 0)
            {
                user.Status = page.UserStatus.Text.Equals("изменить статус") ? null : page.UserStatus.Text;
            }
            if (page.Driver.FindElements(By.XPath(MainPage.MatualFriendsLocator)).Count != 0)
            {
                user.Rate = Convert.ToInt32(page._MatuaFriends.Text);
            }
            if (page.Driver.FindElements(By.XPath(MainPage.WorkInfoLocator)).Count != 0)
            {
                user.Work = page._WorkInfo.Text;
            }
            return user;
        }

        private IEnumerable<string> GetFriendsUrls(string url)
        {
            var pageFriend = NewPage(url).GoToFriendsPage();
            if (pageFriend == null) return null;
            var friends = new List<string>();
            while (friends.Count < 10)
            {
                friends = friends.Union((from friend in pageFriend.FriendsLink
                        where !CheckUser(friend.GetAttribute("href"))
                        select friend.GetAttribute("href")).ToList())
                    .ToList();
                if (pageFriend.GetMoreFriend() == null) break;
            }
            if (friends.Count > 10)
            {
                friends.RemoveRange(10, friends.Count - 10);
            }
            return friends.ToArray();
        }

        private MainPage NewPage(string url)
        {
            return new MainPage(_driver, url).OpenPage();
        }

        private bool CheckUser(string url)
        {
            var flag = false;
            foreach (var us in _users)
            {
                if (us.Url.Equals(url))
                {
                    flag = true;
                }
            }
            return flag;
        }
    }
}