﻿using System.Collections.Generic;
using System.Data.Entity;
using CostEffectiveCode.AutoMapper;
using CostEffectiveCode.Common;
using CostEffectiveCode.Cqrs;
using CostEffectiveCode.Ddd;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using VkService.App.Manager;
using VkService.App.Service;
using VkService.Controllers;
using VkService.DAL;

namespace VkService.App_Start
{
    public static class SimpleInjectorConfig
    {
        public static Container GetContainer()
        {
            var container = new Container();
            container.Options.DefaultLifestyle = new WebRequestLifestyle();

            var reg = new WebRequestLifestyle().CreateRegistration<VkServiceDbContext>(container);
            container.AddRegistration(typeof(ILinqProvider), reg);
            container.AddRegistration(typeof(IUnitOfWork), reg);
            container.AddRegistration(typeof(DbContext), reg);

            var userQueryReg = new WebRequestLifestyle().CreateRegistration<UsersQuery>(container);
            container.AddRegistration(typeof(IQuery<IEnumerable<UserDto>>), userQueryReg);
            container.AddRegistration(typeof(IQuery<int, UserDto>), userQueryReg);
            container.AddRegistration(typeof(IQuery<int, IEnumerable<UserDto>>), userQueryReg);

            var am = Lifestyle.Singleton.CreateRegistration(() => new StaticAutoMapperWrapper(), container);
            container.AddRegistration(typeof(IProjector), am);
            container.AddRegistration(typeof(IMapper), am);

            container.Register<IPageManager,PageManager>();
            container.Register<ILoaderFriends,LoaderFriends>();

            return container;
        }
    }
}