﻿using AutoMapper;
using VkService.Controllers;
using VkService.DAL.Entities;

namespace VkService
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new UserProfile());
            });
        }
    }

    public class UserProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<User, UserDto>();
        }
    }

}