﻿using System.Web.Configuration;
using System;

namespace VkService
{
    public static class Config
    {
        public static readonly string Browser      = WebConfigurationManager.AppSettings["browser"];
        public static readonly int    WaitTimeout  = Convert.ToInt32(WebConfigurationManager.AppSettings["waitTimeout"]);
        public static readonly string Login        = WebConfigurationManager.AppSettings["login"];
        public static readonly string Password     = WebConfigurationManager.AppSettings["password"];
        public static readonly int    Iteration    = Convert.ToInt32(WebConfigurationManager.AppSettings["iteration"]);
    }
}