﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using CostEffectiveCode.Common;
using CostEffectiveCode.Cqrs;
using CostEffectiveCode.Ddd;
using CostEffectiveCode.Ddd.Entities;
using VkService.App.Service;
using VkService.DAL.Entities;

namespace VkService.Controllers
{
    public class VkController : Controller
    {
        private readonly IQuery<IEnumerable<UserDto>> _allUsers;
        private readonly IQuery<int, UserDto> _byId;
        private readonly IQuery<int, IEnumerable<UserDto>> _friendsUser;
        private readonly ILoaderFriends _vkService;

        public VkController(IQuery<IEnumerable<UserDto>> allUsers,
            IQuery<int, UserDto> byId,
            IQuery<int, IEnumerable<UserDto>> friendsUser,
            ILoaderFriends vkService)
        {
            _allUsers = allUsers;
            _byId = byId;
            _friendsUser = friendsUser;
            _vkService = vkService;
        }

        // GET: Vk
        public ActionResult Index()
        {
            var users = _allUsers.Ask();
            return View(users);
        }
        public ActionResult Users(int id)
        {
            var user = _byId.Ask(id);
            ViewBag.Login = user.Login;
            ViewBag.Data = user.DataRunWd;
            ViewBag.Name = user.Name;
            return View(_friendsUser.Ask(id));
        }

        public ActionResult StartService()
        {
            _vkService.Start(Config.Browser,Config.WaitTimeout,Config.Login,Config.Password,Config.Iteration);
            return Index();
        }

        public bool IsRun()
        {
            return LoaderFriends.FlagRun;
        }
    }

    public class UsersQuery :
        IQuery<IEnumerable<UserDto>>,
        IQuery<int, UserDto>,
        IQuery<int, IEnumerable<UserDto>>
    {
        private readonly ILinqProvider _linqProvider;
        private readonly IUnitOfWork _uof;
        private readonly IMapper _mapper;

        public UsersQuery(ILinqProvider linqProvider, IUnitOfWork uof, IMapper mapper)
        {
            _linqProvider = linqProvider;
            _uof = uof;
            _mapper = mapper;
        }

        public IEnumerable<UserDto> Ask()
        {
            return _linqProvider
                .Query<User>()
                .ProjectTo<UserDto>()
                .ToList();
        }

        public UserDto Ask(int id)
        {
            var user =  _uof.Find<User>(id);
            return _mapper.Map<UserDto>(user);
        }

        IEnumerable<UserDto> IQuery<int, IEnumerable<UserDto>>.Ask(int spec)
        {
            return _linqProvider.Query<User>()
                .Where(x => x.ParentId == spec)
                .ProjectTo<UserDto>()
                .ToList();
        }
    }

    public class UserDto : HasIdBase<int>
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Login { get; set; }
        public string Status { get; set; }
        public int Rate { get; set; }
        public string Url { get; set; }
        public DateTime DataRunWd { get; set; }
        public string Work { get; set; }
        public int MatualFriends { get; set; } = 0;
    }
}