﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CostEffectiveCode.Ddd;
using CostEffectiveCode.Ddd.Entities;
using VkService.DAL.Entities;
using VkService.DAL.Migration;

namespace VkService.DAL
{
    public class VkServiceDbContext : DbContext, ILinqProvider, IUnitOfWork
    {
        public DbSet<User> Users { get; set; }

        public VkServiceDbContext() : base("Main")
        {
            Configuration.LazyLoadingEnabled = true;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<VkServiceDbContext, Configuration>());
        }

        public IQueryable<TEntity> Query<TEntity>()
            where TEntity : class, IHasId => Query(typeof(TEntity)).Cast<TEntity>();

        public IQueryable Query(Type t) => Set(t).AsQueryable();

        public void Add<TEntity>(TEntity entity) where TEntity : class, IHasId
            => Set<TEntity>().Add(entity);

        public void Delete<TEntity>(TEntity entity) where TEntity : class, IHasId
            => Set<TEntity>().Remove(entity);

        public TEntity Find<TEntity>(object id) where TEntity : class, IHasId
            => Set<TEntity>().Find(id);

        public IHasId Find(Type entityType, object id)
            => (IHasId) Set(entityType).Find(id);

        public void Commit() => SaveChanges();

        public void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            Set<TEntity>().AddRange(entities);
        }

    }
}