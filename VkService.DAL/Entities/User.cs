﻿using System;
using CostEffectiveCode.Ddd.Entities;

namespace VkService.DAL.Entities
{
    public class User : IHasId<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string Login { get; set; }
        public string Status { get; set; }
        public virtual User Parent { get; set; }
        public int? ParentId { get; set; }
        public int Rate { get; set; } = 0;
        public string Url { get; set; }
        public DateTime DataRunWd { get; set; } = DateTime.Now;
        public string Work { get; set; }
        object IHasId.Id { get; }

    }
}