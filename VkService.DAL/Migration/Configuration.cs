﻿using System.Data.Entity.Migrations;

namespace VkService.DAL.Migration
{
    public class Configuration : DbMigrationsConfiguration<VkServiceDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
    }
}